package revolute.moneyTransferApp;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import static org.easymock.EasyMock.*;

import revolut.moneyTransferApp.dao.AccountDAO;
import revolut.moneyTransferApp.dao.UserDAO;
import revolut.moneyTransferApp.entity.Account;
import revolut.moneyTransferApp.service.AccountService;

@RunWith(GuiceJUnit4Runner.class)
public class AccountServiceTest {

	@Inject private AccountService accountService;
    @Inject private AccountDAO accountDAO;
    @Inject private UserDAO userDAO;

    @Provides @Singleton
    public AccountDAO mockAccountDAO() {
        return createStrictMock(AccountDAO.class);
    }
    
    @Provides @Singleton
    public UserDAO mockUserDAO() {
        return createStrictMock(UserDAO.class);
    }

    
    @Test
    public void creditAmount() {
       
    }
    
    // test with multiple threads
    @Test
    public void transferAmount() {
       
    }
    
    // test rollback
    @Test
    public void createUserAccount() {
       
    }
    	
}
    

