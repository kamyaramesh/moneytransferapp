package revolute.moneyTransferApp;

import static org.easymock.EasyMock.createStrictMock;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import junit.framework.Assert;
import revolut.moneyTransferApp.dao.UserDAO;
import revolut.moneyTransferApp.entity.User;
import revolut.moneyTransferApp.exception.EntityCreationException;
import revolut.moneyTransferApp.service.UserService;

@RunWith(GuiceJUnit4Runner.class)
public class UserServiceTest {

	@Inject private UserService userService;
    @Inject private UserDAO userDAO;
    
    @Provides @Singleton
    public UserDAO mockUserDAO() {
        return createStrictMock(UserDAO.class);
    }

    @Test
	public void fetchUser() {
		try {
			User user = new User();
			user.setFirstName("fn");
			user.setLastName("ln");

			userDAO.createUser(user);
		} catch (EntityCreationException e) {
			Assert.fail();
		}
	}
    	
}