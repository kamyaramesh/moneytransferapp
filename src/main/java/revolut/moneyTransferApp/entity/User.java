package revolut.moneyTransferApp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User extends revolut.moneyTransferApp.entity.Entity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name= "userId", nullable = false)
	private Integer id;
	
	@JoinColumn(name = "account",referencedColumnName = "Id", nullable = false)
	private Account account;
	
	
	@Column(name= "firstname", nullable = false)
	private String firstName;
	
	
	@Column(name= "lastname", nullable = false)
	private String lastName;
	
	
	public User(){
		
	}
	
	public User(Integer id, Account account) {
		super();
		this.id = id;
		this.account = account;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", account=" + account + ", firstName=" + firstName
				+ ", lastName=" + lastName + "]";
	}
		
}
