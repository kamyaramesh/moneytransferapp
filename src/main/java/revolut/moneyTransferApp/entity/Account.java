package revolut.moneyTransferApp.entity;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import revolut.moneyTransferApp.exception.InvalidOperationException;

@Entity
@Table(name="Account")
public class Account extends revolut.moneyTransferApp.entity.Entity  {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "Id")
	private Integer id;
	
	@Column(name = "balance")
	private Double balance;
	
	@Column(name = "type")
	private String accountType;
	

	public Account(){
		
	}
	
	public Account(Integer id, Double balance, String accountType) {
		super();
		this.id = id;
		this.balance = balance;
		this.accountType = accountType;
	}

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public void deductAmount(Double amount) throws InvalidOperationException{
				
		if(amount != null && amount <= this.getBalance()){
			double newBalance = this.getBalance() - amount;
			this.setBalance(newBalance);
		}else{
			throw new InvalidOperationException("Amount cannot be deducted");
		}
	}
	
	public void addAmount(Double amount){
		
		if(amount != null){
			double newBalance = this.getBalance() + amount;
			this.setBalance(newBalance);
		}
	}


	@Override
	public String toString() {
		return "Account [id=" + id + ", balance=" + balance + ", accountType=" + accountType + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountType == null) ? 0 : accountType.hashCode());
		result = prime * result + ((balance == null) ? 0 : balance.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountType == null) {
			if (other.accountType != null)
				return false;
		} else if (!accountType.equals(other.accountType))
			return false;
		if (balance == null) {
			if (other.balance != null)
				return false;
		} else if (!balance.equals(other.balance))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void lock() {
		// TODO Auto-generated method stub
		
	}

	public void lockInterruptibly() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	public boolean tryLock() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	public void unlock() {
		// TODO Auto-generated method stub
		
	}

	public Condition newCondition() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}

