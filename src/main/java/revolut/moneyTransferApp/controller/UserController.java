package revolut.moneyTransferApp.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.google.inject.Inject;

import revolut.moneyTransferApp.entity.Account;
import revolut.moneyTransferApp.entity.User;
import revolut.moneyTransferApp.service.UserService;

@Path("/user")
public class UserController {
	
	@Inject 
	UserService userService;

	@POST
	@Path("/create/userAccount")
	@Produces("text/plain")
	public Response createUserAccount(@QueryParam(value = "fn") String fn, @QueryParam(value = "ln") String ln,
			@QueryParam(value = "minBalance") Double minBalance) {

		try {
			User user = new User();
			user.setFirstName(fn);
			user.setLastName(ln);
			Account account = new Account();
			account.setBalance(minBalance);
			user.setAccount(account);
			user = userService.createUserAccount(user);
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}

	}
	
	@GET
	@Path("/all")
	@Produces("text/plain")
	public String getAllUsers(){
		try{
			return userService.fetchAll().toString();
		}catch(Exception e){
			e.printStackTrace();
			return e.getMessage();
		}
		
		
	}
	
}
