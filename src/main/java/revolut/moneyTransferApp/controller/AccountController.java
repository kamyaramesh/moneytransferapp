package revolut.moneyTransferApp.controller;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import revolut.moneyTransferApp.service.AccountService;

@Path("/account")
public class AccountController {

	@Inject
	AccountService accountService;

	@POST
	@Path("/{id}/credit/{amount}")
	@Produces("text/plain")
	public Response creditAmount(@PathParam(value = "id") Integer accountId,
			@PathParam(value = "amount") Double amount) {
		try {
			accountService.creditAmount(accountId, amount);
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}

	}

	@POST
	@Path("/{id}/debit/{amount}")
	@Produces("text/plain")
	public Response debitAmount(@PathParam(value = "id") Integer accountId,
			@PathParam(value = "amount") Double amount) {

		try {
			accountService.debitAmount(accountId, amount);
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@POST
	@Path("/transfer/{amount}")
	@Produces("text/plain")
	public Response transferAmount(@QueryParam(value = "from") Integer from, @QueryParam(value = "to") Integer to,
			@PathParam(value = "amount") Double amount) {

		try {
			accountService.transferAmount(from, to, amount);
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

	@GET
	@Path("/all")
	@Produces("text/plain")
	public String getAllAccounts() {

		return accountService.getAllAccounts().toString();
	}

}
