package revolut.moneyTransferApp.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import revolut.moneyTransferApp.entity.Account;
import revolut.moneyTransferApp.exception.TransactionException;
import revolut.moneyTransferApp.exception.EntityCreationException;

import javax.persistence.OptimisticLockException;

public class AccountDAO extends GenericDAO{
	
	
	@Transactional(rollbackOn = EntityCreationException.class) 
	public Account createAccount(Account account) throws EntityCreationException {

		try {
			em.getTransaction().begin();
			em.persist(account);
			em.flush();
			return account;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new EntityCreationException("Account could not be created : " + e.getMessage());
		}

	}
	
	@Transactional(rollbackOn = {OptimisticLockException.class,TransactionException.class}) 
	public void creditAmount(Integer accountId, Double amount) throws TransactionException{
		try{
			Account acc = em.find(Account.class, accountId);
			acc.addAmount(amount);
			saveOrUpdate(acc);
		}catch(Exception e){
			em.getTransaction().rollback();
			throw new TransactionException("Exception while crediting amount : " + e.getMessage());
		}
		
	}
	
	@Transactional(rollbackOn = {OptimisticLockException.class ,TransactionException.class}) 
	public void debitAmount(Integer accountId, Double amount) throws TransactionException{
		try{
			Account acc = em.find(Account.class, accountId);
			acc.deductAmount(amount);
			saveOrUpdate(acc);
		}catch(Exception e){
			em.getTransaction().rollback();
			throw new TransactionException("Exception while debiting amount : " + e.getMessage());
		}
		
	}
	
	@Transactional(rollbackOn = {OptimisticLockException.class ,TransactionException.class}) 
	public void transferAmount(Integer fromAccount, Integer toAccount, Double amount) throws TransactionException{
		try{
			Account fromAcc = em.find(Account.class, fromAccount);
			fromAcc.deductAmount(amount);
			Account toAcc = em.find(Account.class, toAccount);
			toAcc.addAmount(amount);
			em.getTransaction().begin();
			em.persist(fromAcc);
			em.persist(toAcc);
			em.getTransaction().commit();
			
		}catch(Exception e){
			em.getTransaction().rollback();
			throw new TransactionException("Exception while transfering amount : " + e.getMessage());
		}	
	}
	
	@Transactional
	public List<Account> getAllAccounts(){
		Query query = em.createQuery("SELECT a FROM Account a");
		List resultList = query.getResultList();
	    return (List<Account>) query.getResultList();
		
	}
	
	public Account getAccount(Integer accountId) throws TransactionException{
		Account acc = null;
		try{
			acc = em.find(Account.class, accountId);
			
		}catch(Exception e){
			em.getTransaction().rollback();
			throw new TransactionException("Exception while fetching acc : " + e.getMessage());
		}
		
		return acc;
	}
	
}
