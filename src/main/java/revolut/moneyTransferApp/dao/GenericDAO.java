package revolut.moneyTransferApp.dao;

import javax.persistence.EntityManager;

import com.google.inject.Inject;

public abstract class GenericDAO {

	public GenericDAO() {
		// TODO Auto-generated constructor stub
	}
	
	@Inject EntityManager em; 
	
	public void saveOrUpdate(revolut.moneyTransferApp.entity.Entity entity){
		em.getTransaction().begin();
		em.persist(entity);
		em.getTransaction().commit();
	}
	
	public void delete(revolut.moneyTransferApp.entity.Entity entity){
		em.getTransaction().begin();
		em.remove(entity);
		em.getTransaction().commit();
	}
	

}
