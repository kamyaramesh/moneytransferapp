package revolut.moneyTransferApp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import revolut.moneyTransferApp.entity.User;
import revolut.moneyTransferApp.exception.EntityCreationException;


public class UserDAO extends GenericDAO {
	
	
	public List<User> fetchAll(){
		Query query = em.createQuery("SELECT u FROM User u");
		List resultList = query.getResultList();
	    return (List<User>) query.getResultList();
	}
	
	public User fetchUser(Integer userId){
		Query query = em.createQuery("SELECT u FROM User u where u.userId = :userId");
	    return (User) query.getSingleResult();
	}

	public void deleteUser(Integer userId){
		
	}
	
	public void updateUser(User user){
		
	}
	
	@Transactional(rollbackOn = {EntityCreationException.class,Exception.class})
	public User createUser(User user) throws EntityCreationException{
		try{
			em.persist(user);
			em.getTransaction().commit();
		}catch(Exception e){
			em.getTransaction().rollback();
			throw new EntityCreationException("User could not be created : "+e.getMessage());
		}
		
		return user;
	}
	

}
