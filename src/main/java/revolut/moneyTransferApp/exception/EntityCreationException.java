package revolut.moneyTransferApp.exception;

public class EntityCreationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EntityCreationException(String message) {
        super(message);
    }

}
