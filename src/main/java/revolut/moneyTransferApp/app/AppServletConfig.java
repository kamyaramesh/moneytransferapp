package revolut.moneyTransferApp.app;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import revolut.moneyTransferApp.controller.AccountController;
import revolut.moneyTransferApp.controller.TransferController;
import revolut.moneyTransferApp.controller.UserController;
import revolut.moneyTransferApp.dao.AccountDAO;
import revolut.moneyTransferApp.dao.UserDAO;
import revolut.moneyTransferApp.service.AccountService;
import revolut.moneyTransferApp.service.MoneyTransferService;
import revolut.moneyTransferApp.service.UserService;

public class AppServletConfig extends GuiceServletContextListener{

	@Override
	protected Injector getInjector() {
		return Guice.createInjector(new JerseyServletModule() {
			@Override
			protected void configureServlets() {
				bind(AccountService.class).in(Scopes.SINGLETON);
				bind(MoneyTransferService.class).in(Scopes.SINGLETON);
				bind(UserService.class).in(Scopes.SINGLETON);
				bind(AccountController.class).in(Scopes.SINGLETON);
				bind(TransferController.class).in(Scopes.SINGLETON);
				bind(UserController.class).in(Scopes.SINGLETON);
				bind(AccountDAO.class).in(Scopes.SINGLETON);
				bind(UserDAO.class).in(Scopes.SINGLETON);
				install(new JpaPersistModule("myFirstJpaUnit"));  // like we saw earlier.
			    filter("/*").through(PersistFilter.class);
				serve("/*").with(GuiceContainer.class);
			}
		});
	}

}
