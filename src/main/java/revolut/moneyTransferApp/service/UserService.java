package revolut.moneyTransferApp.service;

import java.util.List;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import revolut.moneyTransferApp.dao.AccountDAO;
import revolut.moneyTransferApp.dao.UserDAO;
import revolut.moneyTransferApp.entity.Account;
import revolut.moneyTransferApp.entity.User;
import revolut.moneyTransferApp.exception.EntityCreationException;

public class UserService {
	
	@Inject
	UserDAO userDAO;
	
	@Inject
	AccountDAO accountDAO;
	
	public User fetchUser(User user){
		
		return null;
	}
	
	public List<User> fetchAll(){
		List<User> list = userDAO.fetchAll();
		return list;
	}

	public void deleteUserAccount(Integer userId){
		
	}
	
	public void updateUserDetails(User user){
		
	}
	
	@Transactional(rollbackOn = EntityCreationException.class)
	public User createUserAccount(User user) throws EntityCreationException{
		
		Account account = accountDAO.createAccount(user.getAccount());
		user.setAccount(account);
		User ua = userDAO.createUser(user);
		return ua;
	}
}
