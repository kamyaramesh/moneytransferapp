package revolut.moneyTransferApp.service;

import java.util.List;

import com.google.inject.Inject;

import revolut.moneyTransferApp.dao.AccountDAO;
import revolut.moneyTransferApp.dao.UserDAO;
import revolut.moneyTransferApp.entity.Account;
import revolut.moneyTransferApp.entity.User;
import revolut.moneyTransferApp.exception.TransactionException;

public class AccountService {
	
	@Inject
	AccountDAO accountDAO;
	
	
	
	public List<Account> getAllAccounts(){
		return accountDAO.getAllAccounts();
	}
	
	public void deleteAccount(User user){
		
	}
	
	public void updateAccount(User user, Account account){
		
	}
	
	public void addAccount(User user){
		
	}
	
	public void getAccount(Integer accountId){
		
	}
	
	public void getAccounts(List<Integer> accountIdList){
		
	}
	
	public void creditAmount(Integer accountId, Double amount) throws TransactionException{
		accountDAO.creditAmount(accountId, amount);
	}
	
	public void debitAmount(Integer accountId, Double amount) throws TransactionException{
		accountDAO.debitAmount(accountId, amount);
	}
	
	public void transferAmount(Integer fromAccount, Integer toAccount, Double amount) throws TransactionException{
		accountDAO.transferAmount(fromAccount, toAccount, amount);
	}

}
